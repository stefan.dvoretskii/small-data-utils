import os
from time import perf_counter

def count_files(directory):
    recorded_formats = ["nii", "dcm", "nrrd", "json", "csv", "tsv", "other"]
    file_counts = dict.fromkeys(recorded_formats, 0)

    for root, dirs, files in os.walk(directory):
        for file in files:
            dotsplits = file.split(".")
            recorded_format = False
            for f in recorded_formats:
                if f in dotsplits: 
                    file_counts[f] +=1 
                    recorded_format = True
            if not recorded_format:
                file_counts['other'] += 1
                print("Not recorded format file found: %s" % file)

    return file_counts

start_tstmp = perf_counter()
images_dir = 'W:/Projects/2021_Silvia_COPDGene/from_Oliver/images'
counts_dict = count_files(images_dir)
end_tstmp = perf_counter()


print(counts_dict)
print("Elapsed: %d s" % (end_tstmp - start_tstmp))
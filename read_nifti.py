# see https://nipy.org/nibabel/nifti_images.html 
import nibabel as nib

nifti_path = r"W:\Projects\2023_Silvia_NSCLC\images\Exp\110500.nii.gz"
nifti_ex = nib.load(nifti_path)
print(nifti_ex.header)

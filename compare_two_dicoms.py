#	Print differences in common tags across two DICOM files.
#
#	author: Stefan Dvoretskii <stefan.dvoretskii@dkfz-heidelberg.de>
#


import pydicom
import sys
from pydicom.tag import Tag

def compare_dicom_files(file1_path, file2_path):
    # Load DICOM files
    dicom1 = pydicom.dcmread(file1_path)
    dicom2 = pydicom.dcmread(file2_path)

    # Get common data elements
    common_tags = set(dicom1.keys()) & set(dicom2.keys())
    common_tags.remove(Tag("PixelData"))
    if Tag("DataSetTrailingPadding") in common_tags:
        common_tags.remove(Tag("DataSetTrailingPadding"))

    # Compare values for common data elements
    different_fields = {}
    for tag in common_tags:
        value1 = dicom1[tag].value
        value2 = dicom2[tag].value
        if value1 != value2:
            different_fields[tag] = (dicom1[tag].name, value1, value2)

    # Print the differences
    if different_fields:
        print("Differences found in the following data fields:")
        for tag, values in different_fields.items():
                print(f"Tag: {tag} - {values[0]}, Value in File 1: {values[1]}, Value in File 2: {values[2]}\n")
    else:
        print("No differences found in common data fields.")

# Example usage:
file1_path = sys.argv[1]
file2_path = sys.argv[2]
compare_dicom_files(file1_path, file2_path)

#	Connect to OAI endpoints of Helmholtz libs and get publication / data info.
#
#	author: Stefan Dvoretskii <stefan.dvoretskii@dkfz-heidelberg.de>
#
from oaipmh.client import Client
from oaipmh.metadata import MetadataRegistry, oai_dc_reader

URL = 'https://pub.dzne.de/oai2d'
registry = MetadataRegistry()
registry.registerReader('oai_dc', oai_dc_reader)
client = Client(URL, registry)

i=0
while i < 50:
	print(next(client.listRecords(metadataPrefix = 'oai_dc')))

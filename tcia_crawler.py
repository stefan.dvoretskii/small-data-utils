# see https://wiki.cancerimagingarchive.net/display/Public/TCIA+Programmatic+Interface+REST+API+Guides for (REST= API introduction
# TODO tcia_utils grab from v1 nbia-api by default; consider switching to v2?
from tcia_utils import nbia
import pandas as pd

coll = nbia.getCollections()

def find_counts_nbia(modality, list_of_dicts):
    for l in list_of_dicts:
        if l['criteria'] == modality:
            return int(l['count'])
    return 0

modalitiesByCollection = {}
for entry in coll:
    c = entry['Collection']
    print("Collection: %s" % c)
    mc = nbia.getModalityCounts(collection=c)
    print(mc)

    modalitiesByCollection[c] = [find_counts_nbia('CT', mc), find_counts_nbia('MR', mc)]

cases_counts = pd.DataFrame.from_dict(modalitiesByCollection, orient='index', columns=['CT', 'MR'])
cases_counts.to_csv("cases_counts.tsv",  sep='\t')

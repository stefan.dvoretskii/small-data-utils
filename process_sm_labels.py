#	Automatically detect label images in digital microscopy DICOM directories
#   and use Optical Character Recognition to read label text.
#
#	author: Stefan Dvoretskii <stefan.dvoretskii@dkfz-heidelberg.de>
#


import os
import sys
import pydicom
from PIL import Image
import pytesseract
import numpy as np

# Set the path to the directory containing DICOM images
dicom_directory = sys.argv[1]

def extract_text_from_image(dataset):
    array = np.array(dataset.pixel_array, dtype=np.uint8)
    img = Image.fromarray(array)
    text = pytesseract.image_to_string(img)
    return text

def process_dicom_files(directory):
    for filename in os.listdir(directory):
        filepath = os.path.join(directory, filename)

        if os.path.isfile(filepath) and filename.lower().endswith('.dcm'):
            ds = pydicom.dcmread(filepath)

            if ds.Modality == 'SM' and 'ImageType' in ds and 'LABEL' in ds.ImageType:
                labeled_text = extract_text_from_image(ds)

                print(f"Label for {filename}:")
                print(labeled_text)
                print("=" * 50)

# Process DICOM files in the specified directory
process_dicom_files(dicom_directory)
